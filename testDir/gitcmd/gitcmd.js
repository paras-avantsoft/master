#! /usr/local/bin/node

// requires
var shell = require('shelljs'),
    colors = require('colors'),
    lib = require('./lib/lib.js'); // helper functions

// args
var type,
    help = false,
    remove = false,
    add = false,
    removeAll = false,
    addAll = false,
    addToDev = false;

process.argv.forEach(function(arg) {
    //console.log(arg);
    switch (arg) {
        case 'help':
            help = true;
            break;
        case 'remove':
            remove = true;
            type = arg;
            break;
        case 'add':
            add = true;
            type = arg;
            break;
        case 'removeall':
            removeAll = true;
            type = arg;
            break;
        case 'addall':
            addAll = true;
            type = arg;
            break;
        case 'addtodev':
            addToDev = true;
            type = arg;
            break;
    }
});

//validate arguments
if (!type || help) {
    if (!help) {
        console.log('ERROR:'.red.bold + ' Please specify any of the following valid arguments:'.red.bold);
    }

    console.log('# Command Help...'.green.bold);
    console.log('');
    console.log('  remove    :'.yellow.bold + ' Specify "remove" command to remove multiple directory or files');
    console.log('             Add directory/file path in "dirArray" of build/gitcmd.js');
    console.log('             then execute bellow command');
    console.log('             >> $ node gitcmd.js remove'.green.bold);
    console.log('');
    console.log('  add       :'.yellow.bold + ' Specify "add" command to add multiple directory or files');
    console.log('             Add directory/file path in "dirArray" of build/gitcmd.js');
    console.log('             then execute bellow command');
    console.log('             >> $ node gitcmd.js add'.green.bold);
    console.log('');
    console.log('  removeall :'.yellow.bold + ' Specify "removeall" command to delete all cards');
    console.log('             No need to specify any path just execute bellow command');
    console.log('             >> $ node gitcmd.js removeall'.green.bold);
    console.log('');
    console.log('  addall    :'.yellow.bold + ' Specify "addall" command to add all cards');
    console.log('             No need to specify any path just execute bellow command');
    console.log('             >> $ node gitcmd.js addall'.green.bold);
    console.log('');
    console.log('  addtodev  :'.yellow.bold + ' Specify "addtodev" command to add cards from master to dev');
    console.log('             Add directory/file path in "dirArray" of build/gitcmd.js');
    console.log('             then execute bellow command');
    console.log('             >> $ node gitcmd.js addtodev'.green.bold);

    return;
}

gitCommand = {

    run: function() {
        var me = this;

        me.runGitCommand();
    },

    runGitCommand: function() {
        var me = this,
            removeIt,
            addItMaster,
            dirArray,
            addItDev,
            cmdWithPath = [];

        removeIt = 'git rm -r ';
        addItMaster = 'git checkout dev -- ';
        addItDev = 'git checkout master -- ';
        dirArray = [
            // Path example : '../../testDir/cardTestDir/latestCard1'
            '../../testDir/cardTestDir/latestCard1_'
        ];

        if (remove || add || removeAll || addAll || addToDev) {
            console.log('Running...'.green.bold);

            if (dirArray.length === 0 && !remove && !add && !addToDev) {
                cmdWithPath = removeAll ? ['git rm -r ../../testDir/cardTestDir'] : ['git checkout dev -- ../../testDir/cardTestDir'];
            } else {

                if (dirArray.length === 0) {
                    console.log('Please add directory/file path in "dirArray" in build/gitcmd.js and again execute command'.red.bold);
                    return;
                }

                var cmdType = !cmdType && remove ? removeIt : (addToDev ? addItDev : addItMaster);
                for (l = 0; l < dirArray.length; l++) {
                    cmdWithPath[l] = cmdType + dirArray[l];
                }
            }
            
            cmdWithPath && me.executeCommand(cmdWithPath);
        }
    },


    executeCommand: function(cmdWithPath) {
        // pop element from array
        // based on that array element process your command
        // in the callback call the same method again. 
        if (cmdWithPath.length === 0) {
            return;
        }
        var me = this,
            commandPath = cmdWithPath.pop();

        console.log(commandPath.yellow);

        lib.shell(commandPath, function() {
            me.executeCommand(cmdWithPath);
        });
    }
};

// will call run function to start execution of command
gitCommand.run();