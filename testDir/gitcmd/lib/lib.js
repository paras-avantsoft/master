var fs = require('fs'),
    shell = require('shelljs'),
    spawn = require('child_process').spawn;

exports.shell = function (command, exitCallback) {
    var args = command.split(' '),
        cmd = args.shift(),
        shellProcess;

    shellProcess = spawn(cmd, args, { stdio: 'inherit' });

    shellProcess.on('exit', function (code) {
        if (exitCallback) {
            exitCallback.apply(this, arguments);
        }
    });
};